package com.example.calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    boolean isComma = false;
    boolean isPlus = false;
    boolean isDiv = false;
    boolean isMin = false;
    boolean isMult = false;
    Double resultat;
    Double operator1;
    String operation;
    Double operator2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //Instantiate all the views.
        TextView operationText = (TextView) findViewById(R.id.operationText);
        TextView result = (TextView) findViewById(R.id.txtRes);
        /*Button btnMc = (Button) findViewById(R.id.btnMc);
        Button btnMr = (Button) findViewById(R.id.btnMr);
        Button btnMplus = (Button) findViewById(R.id.btnMplus);
        Button btnMmin = (Button) findViewById(R.id.btnMmin);
        Button btnMs = (Button) findViewById(R.id.btnMs);
        Button btnM = (Button) findViewById(R.id.btnM);
        Button btnPercent = (Button) findViewById(R.id.btnPercent);*/
        Button btnCe = (Button) findViewById(R.id.btnCe);
        Button btnC = (Button) findViewById(R.id.btnC);
        ImageButton delete = (ImageButton) findViewById(R.id.delete);
        Button btnUEntreX = (Button) findViewById(R.id.btnUEntreX);
        Button btnXQuad = (Button) findViewById(R.id.btnXQuad);
        Button btnSqurArrel = (Button) findViewById(R.id.btnSqurArrel);
        Button btnDiv = (Button) findViewById(R.id.btnDiv);
        Button btnSeven = (Button) findViewById(R.id.btnSeven);
        Button btnEight = (Button) findViewById(R.id.btnEight);
        Button btnNine = (Button) findViewById(R.id.btnNine);
        Button btnSix = (Button) findViewById(R.id.btnSix);
        Button btnFive = (Button) findViewById(R.id.btnFive);
        Button btnFour = (Button) findViewById(R.id.btnFour);
        Button btnThree = (Button) findViewById(R.id.btnThree);
        Button btnTwo = (Button) findViewById(R.id.btnTwo);
        Button btnOne = (Button) findViewById(R.id.btnOne);
        Button btnMult = (Button) findViewById(R.id.btnMult);
        Button btnMin = (Button) findViewById(R.id.btnMin);
        Button btnPlus = (Button) findViewById(R.id.btnPlus);
       // Button btnPlusMin = (Button) findViewById(R.id.btnPlusMin);
        Button btnZero = (Button) findViewById(R.id.btnZero);
        Button btnComma = (Button) findViewById(R.id.btnComma);
        Button btnEqual = (Button) findViewById(R.id.btnEqual);





        //Create onClick event listener
        View.OnClickListener ocl = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int clickBtnId = view.getId();

                switch(clickBtnId){
                    case R.id.btnNine:
                        if(operation!=null){
                            result.setText("");
                            operation = null;
                        }
                        result.setText(result.getText()+"9");
                        break;
                    case R.id.btnEight:
                        if(operation!=null){
                            result.setText("");
                            operation = null;
                        }
                        result.setText(result.getText()+"8");
                        break;
                    case R.id.btnSeven:
                        if(operation!=null){
                            result.setText("");
                            operation = null;
                        }
                        result.setText(result.getText()+"7");
                        break;
                    case R.id.btnSix:
                        if(operation!=null){
                            result.setText("");
                            operation = null;
                        }
                        result.setText(result.getText()+"6");
                        break;
                    case R.id.btnFive:
                        if(operation!=null){
                            result.setText("");
                            operation = null;
                        }
                        result.setText(result.getText()+"5");
                        break;
                    case R.id.btnFour:
                        if(operation!=null){
                            result.setText("");
                            operation = null;
                        }
                        result.setText(result.getText()+"4");
                        break;
                    case R.id.btnThree:
                        if(operation!=null){
                            result.setText("");
                            operation = null;
                        }
                        result.setText(result.getText()+"3");
                        break;
                    case R.id.btnTwo:
                        if(operation!=null){
                            result.setText("");
                            operation = null;
                        }
                        result.setText(result.getText()+"2");
                        break;
                    case R.id.btnOne:
                        if(operation!=null){
                            result.setText("");
                            operation = null;
                        }
                        result.setText(result.getText()+"1");
                        break;
                    case R.id.btnZero:
                        if(operation!=null){
                            result.setText("");
                            operation = null;
                        }
                        result.setText(result.getText()+"0");
                        break;
                    case R.id.btnComma:
                        if(operation!=null){
                            result.setText("");
                            operation = null;
                        }
                        if(!isComma) {
                            result.setText(result.getText() + ".");
                            isComma = true;
                        }
                        break;
                    case R.id.delete:
                        if(result.getText().length()>0) {
                            if(result.getText().charAt(result.getText().length()-1)=='.')
                            {
                                isComma = false;
                            }
                            else if(result.getText().charAt(result.getText().length()-1)=='+')
                            {
                                isPlus = false;
                            }
                            result.setText(result.getText().subSequence(0, result.getText().length() - 1));
                        }
                        break;
                    case R.id.btnPlus:
                        if(result.getText().length()>0 && (operation==null || operation=="equal")){
                            operator1 = Double.parseDouble((String) result.getText());
                            operation = "+";
                            operationText.setText(result.getText() + "+");
                            isPlus = true;
                            isComma = false;
                        }
                        break;
                    case R.id.btnDiv:
                        if(result.getText().length()>0 && (operation==null || operation=="equal")){
                            operator1 = Double.parseDouble((String) result.getText());
                            operation = "/";
                            operationText.setText(result.getText() + "/");
                            isDiv = true;
                        }
                        break;
                    case R.id.btnMin:
                        if(result.getText().length()>0 && (operation==null || operation=="equal")){
                            operator1 = Double.parseDouble((String) result.getText());
                            operation = "-";
                            operationText.setText(result.getText() + "-");
                            isMin = true;
                        }
                        break;
                    case R.id.btnMult:
                        if(result.getText().length()>0 && (operation==null || operation=="equal")){
                            operator1 = Double.parseDouble((String) result.getText());
                            operation = "*";
                            operationText.setText(result.getText() + "*");
                            isMult = true;
                        }
                        break;
                    case R.id.btnC:
                        operationText.setText(null);
                        result.setText(null);
                        operation = null;
                        operator2 = null;
                        operator1 = null;
                        break;
                    case R.id.btnCe:
                        result.setText(null);
                        break;
                    case R.id.btnSqurArrel:
                        Double d = Math.sqrt(Double.parseDouble((String) result.getText()));
                        result.setText(d.toString());
                        operation = "equal";
                        break;
                    case R.id.btnXQuad:
                        Double d1 = Math.pow(Double.parseDouble((String) result.getText()), 2);
                        result.setText(d1.toString());
                        operation = "equal";
                        break;
                    case R.id.btnUEntreX:
                        Double d2 = 1 / Double.parseDouble((String) result.getText());
                        result.setText(d2.toString());
                        operation = "equal";
                        break;
                    case R.id.btnEqual:
                            operator2 = Double.parseDouble((String)result.getText());
                            operationText.setText(operationText.getText().toString()+result.getText().toString()+"=");
                            Double resultat = 0d;
                            if(isPlus)
                            {
                                resultat = operator1 + operator2;
                                operation = null;
                            }else if(isMin)
                            {
                                resultat = operator1 - operator2;
                                operation = null;
                            }
                            else if(isDiv)
                            {
                                resultat = operator1 / operator2;
                                operation = null;
                            }
                            else if(isMult)
                            {
                                resultat = operator1 * operator2;
                                operation = null;
                            }
                            result.setText(resultat.toString());
                            isComma = false;
                            operation = "equal";
                            isPlus = false;
                            isDiv = false;
                            isMult = false;
                            isMin = false;
                            operator1 = null;
                            operator2 = null;

                        break;
                }
            }
        };

        //Set onClick eventListener
        btnNine.setOnClickListener(ocl);
        btnEight.setOnClickListener(ocl);
        btnSeven.setOnClickListener(ocl);
        btnSix.setOnClickListener(ocl);
        btnFive.setOnClickListener(ocl);
        btnFour.setOnClickListener(ocl);
        btnThree.setOnClickListener(ocl);
        btnTwo.setOnClickListener(ocl);
        btnOne.setOnClickListener(ocl);
        btnZero.setOnClickListener(ocl);
        btnComma.setOnClickListener(ocl);
        delete.setOnClickListener(ocl);
        btnPlus.setOnClickListener(ocl);
        btnEqual.setOnClickListener(ocl);
        btnDiv.setOnClickListener(ocl);
        btnMult.setOnClickListener(ocl);
        btnMin.setOnClickListener(ocl);
        btnC.setOnClickListener(ocl);
        btnCe.setOnClickListener(ocl);
        btnSqurArrel.setOnClickListener(ocl);
        btnXQuad.setOnClickListener(ocl);
        btnUEntreX.setOnClickListener(ocl);











    }
}