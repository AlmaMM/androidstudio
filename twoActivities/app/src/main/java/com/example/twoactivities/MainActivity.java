package com.example.twoactivities;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    String tag = "Log";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.i(tag, "I'm onCreate");

        //Declaring the references to the usable Views:
        EditText userName = (EditText) findViewById(R.id.editTextTextPersonName);
        Button go = (Button) findViewById(R.id.button);

        //Set one onClickListener to the button:
        go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Instantiate Intent for the child activity to launch with the applicationContext
                // and the child class file:
                Intent i = new Intent(getApplicationContext(), babyActivity.class);

                //Intantiate a Bundle. It can contain primitive types and other classes objects. With
                //this class we can send information between activities.
                Bundle bundle = new Bundle();
                bundle.putString("NAME", userName.getText().toString());
                i.putExtras(bundle);

                startActivityForResult(i,1);


            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==1)
        {
            if(resultCode == Activity.RESULT_OK)
            {
                Toast.makeText(this, data.getStringExtra("RESULT").toString(), Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(this, "NOT GOOD, NOT GOOD...", Toast.LENGTH_LONG).show();
            }
        }
        else
        {
            Toast.makeText(this, "WHAT?¿?¿?¿?¿", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onRestart() {
        Log.i(tag, "I'm onRestart");
        super.onRestart();
    }

    @Override
    protected void onStart() {
        Log.i(tag, "I'm onStart");
        super.onStart();
    }

    @Override
    protected void onResume() {
        Log.i(tag, "I'm onResume");
        super.onResume();
    }

    @Override
    protected void onPause() {
        Log.i(tag, "I'm onPause");
        super.onPause();
    }

    @Override
    protected void onStop() {
        Log.i(tag, "I'm onStop");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.i(tag, "I'm onDestroy");
        super.onDestroy();
    }
}