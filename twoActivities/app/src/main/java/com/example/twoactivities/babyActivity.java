package com.example.twoactivities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class babyActivity extends AppCompatActivity implements View.OnClickListener {
    String tag = "babyLog";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(tag, "I'm onCreate baby");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_baby);

        TextView txt = (TextView) findViewById(R.id.textView);
        Button accept = (Button) findViewById(R.id.button4);
        Button cancel = (Button) findViewById(R.id.button3);
        accept.setOnClickListener(this);
        cancel.setOnClickListener(this);

        Bundle bundle = this.getIntent().getExtras();
        txt.setText("HOLA "+bundle.getString("NAME")+", ¿ACEPTAS ACEPTAR?");

    }

    @Override
    protected void onStart() {
        Log.i(tag, "I'm onStart baby");
        super.onStart();
    }

    @Override
    protected void onRestart() {
        Log.i(tag, "I'm onRestart baby");
        super.onRestart();
    }

    @Override
    protected void onResume() {
        Log.i(tag, "I'm onResume baby");
        super.onResume();
    }


    @Override
    protected void onPause() {
        Log.i(tag, "I'm onPause baby");
        super.onPause();
    }


    @Override
    protected void onStop() {
        Log.i(tag, "I'm onStop baby");
        super.onStop();
    }


    @Override
    protected void onDestroy() {
        Log.i(tag, "I'm onDestroy baby");
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        Intent i = new Intent();
        switch(view.getId())
        {
            case R.id.button4:
                i.putExtra("RESULT", "Aceptas aceptar...");
                setResult(Activity.RESULT_OK, i);
                finish();
                break;
            case R.id.button3:
                i.putExtra("RESULT", "No has aceptado aceptar... Ok.");
                setResult(Activity.RESULT_OK, i);
                finish();
                break;
            default:
                break;
        }
    }


}
