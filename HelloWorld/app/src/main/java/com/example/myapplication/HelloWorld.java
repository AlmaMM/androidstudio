package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Paint;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.EventListener;

public class HelloWorld extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btn_show = (Button) findViewById(R.id.btn_show);
        Button btn_hide = (Button) findViewById(R.id.btn_hide);
        Button aumentar = (Button) findViewById(R.id.aumentar);
        Button reducir = (Button) findViewById(R.id.reducir);
        Button aqua = (Button) findViewById(R.id.aqua);
        Button fucsia = (Button) findViewById(R.id.fucsia);
        Button lime = (Button) findViewById(R.id.lime);
        Button fiesta = (Button) findViewById(R.id.fiesta);


        TextView txt_hiBb = (TextView) findViewById(R.id.lbl_hiBb);

        btn_show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txt_hiBb.setVisibility(TextView.VISIBLE);
            }
        });

        btn_hide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txt_hiBb.setVisibility(TextView.INVISIBLE);
                txt_hiBb.setVisibility((TextView.GONE));
            }
        });
        aumentar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                float size = txt_hiBb.getTextSize();
                size+=10;
                txt_hiBb.setTextSize(TypedValue.COMPLEX_UNIT_PX, size);
            }
        });

        reducir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                float size = txt_hiBb.getTextSize();
                size-=10;
                txt_hiBb.setTextSize(TypedValue.COMPLEX_UNIT_PX, size);
            }
        });

        aqua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                txt_hiBb.setBackgroundColor(getResources().getColor(R.color.yellow));
                txt_hiBb.setTextColor(getResources().getColor(R.color.aqua));
            }
        });
        fucsia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                txt_hiBb.setBackgroundColor(getResources().getColor(R.color.blue));
                txt_hiBb.setTextColor(getResources().getColor(R.color.fucsia));
            }
        });
        lime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                txt_hiBb.setBackgroundColor(getResources().getColor(R.color.red));
                txt_hiBb.setTextColor(getResources().getColor(R.color.lime));
            }
        });

        fiesta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fiesta.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        lime.performClick();
                    }
                }, 2000);
                fiesta.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        fucsia.performClick();
                    }
                }, 3000);
                fiesta.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        aqua.performClick();
                    }
                }, 4000);

            }
        });






    }
}