package com.example.uf1_1_a13_spinner_intents_2;


import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import android.support.v4.app.NavUtils;

public class MainActivity extends Activity {

	Button cmdEnviar;
	String[] ArrayProba = new String[] {"Uno","Dos","Tres"};
	
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        // Enlazamos el sprinner al array declarado en tiempo de ejecucion
        Spinner spiProva =(Spinner)findViewById(R.id.spiProva);
        ArrayAdapter<String> AA= new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, ArrayProba );
        spiProva.setAdapter(AA);
        
        
        
        
        cmdEnviar= (Button) findViewById(R.id.cmdEnviar);
        cmdEnviar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			 	 EditText campoNombre = (EditText) findViewById(R.id.txtNombre);
				  //Esto es el asunto de hecho:
			    	String nombre = campoNombre.getText().toString();//Aqu� va el correo destinatario:
			    	EditText campoEmail = (EditText) findViewById(R.id.txtmail);
			    	String email = campoEmail.getText().toString();
			    	//Tipo de mail que vamos a enviar:
			    	Spinner tipoConsulta = (Spinner) findViewById(R.id.spiForma);
			    	String tipo = tipoConsulta.getSelectedItem().toString();
			    	//Mensaje que vamos a enviar:
			    	EditText campoMensaje = (EditText) findViewById(R.id.txtMensaje);
			    	String mensaje = campoMensaje.getText().toString();
			    	//Con este checkbox miramos si el usuario quiere un gatito en el mensaje...
			    	CheckBox checkboxUsarCorreo = (CheckBox) findViewById(R.id.cbUsarMail);  
			    	boolean bUsarCorreo = checkboxUsarCorreo.isChecked();
			    	
			    	/** Enviaremos un eMail a travs de una Activity de la aplicacin de correo, con un Intent*/
			    	Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
					//Con el ACTION_SENDTO + setData(Uri.parse("mailto:")) nos aseguramos que solo aparezcan
					//aplizaciones de CORREO para enviar el mensaje.
					emailIntent.setData(Uri.parse("mailto:"));
			        //emailIntent.setType("plain/text");
			        
			        /** Usar el eMail de destino*/
					//Generamos una String[] con los valores del correo en el camo "Enviar a":
					String[] receivers = new String[]{email};
			        emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,
			            receivers);
			        
			        /** Usar el propio eMail para recibir una copia*/
			        if (bUsarCorreo)
			             mensaje+= " (^._.^) ";
			        
			        /** Estableceremos el asunto*/
					//Para ello tomaremos la informaci�n del tipo de correo enviado y le a�adiremos
					//con una substring el valor del asunto introducido por el usuario. Si es urgente,
					//se a�adir� en may�sculas.
			        String emailAsunto = tipo+" - "+nombre;
					if(tipo.equals("Urgente"))
					{
						emailAsunto = emailAsunto.toUpperCase();
					}
			        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, emailAsunto);
			        
			        /** Aadimos el texto*/
					//Tal cual lo que se indica, se a�ade el mensaje escrito por el usuario
			        emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, mensaje);
			        
			        /** No aadiremos adjuntos. Pero se indica la forma de hacerlo*/
					//Ojito
			        //emailIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
			        
			        startActivity(Intent.createChooser
			        		(emailIntent, "Envie un correo..."));
			        // CreateChooser fa que si hi ha ms una aplicaci per enviar correu, es pugi escollir quina fer servir.
			       

			       
			}
		});
    }



    
    
   
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

    
}
